const express = require('express');
const path = require('path');
const app = express();
const _ = require('lodash')
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(bodyParser.json())
app.use(cors())


let data = [
    {
        id: 2348238942,
        name: 'Betonada',
        artist: 'Rawayana',
        album: 'Rawa'
    },
    {
        id: 78923877823,
        name: 'Amparito',
        artist: 'Soltura',
        album: 'Saladillo'
    }
]

app.set('port', process.env.PORT || 5000);

// listar todos las canciones
app.get('/songs', (req, res) => {
    res.send(data)
});

//buscar una cancion por X (id)
app.get('/songs/+:id', (req, res) => {

    const { id } = req.params // 0
    //forma 1
    // let mySong = null
    // data.forEach((song, i) => {
    //     if (song.id === +id){
    //         mySong = song
    //     }
    // });
    // const respuesta = mySong === null ? 'este id no existe' : mySong
    // res.send(respuesta)

    //forma 2
    const mySong = _.find(data, { id: Number(id) })
    const error = {
        msg: `El id: ${id} no existe en nuestra bd`,
    }
    res.send(mySong === undefined ? error : mySong)
})

// agregar una nueva cancion a bd
app.post('/songs', (req, res) => {
    const body = req.body
    body.id = data.length

    //forma1
    // if (body.name === '' || body.name === undefined)
    //     return res.send('mira falta el nombre')


    // if (body.album === ''|| body.album === undefined)
    //     return res.send('mira falta el album')


    // if (body.artist === ''|| body.artist === undefined)
    //     return res.send('mira falta el artist')

    //data.push(body)
    // console.log('data', data)
    // res.send('mira la cancion se agrego')


    //forma 2
    if (body.name === '' || body.name === undefined || body.album === '' || body.album === undefined || body.artist === '' || body.artist === undefined)
        res.send('Parametros requeridos: name, album, artist')
    else {
        data.push(body)
        res.send('mira la cancion se agrego')
    }

})

// eliminar una cancion dado un id
app.delete('/songs/+:id', (req, res) => {

    const { id } = req.params
    const mySongIndex = _.findIndex(data, { id: Number(id) })

    //data = [4,2,5]
    // data.splice(1, 1) ===> [4,5]
    // data.slice(-1, 1) ===> []

    if (mySongIndex === -1)
        return res.send(`mira este id ${id} no existe`)

    data.splice(mySongIndex, 1)

    res.send(data)

})

// modificar parcial una cancion dado un id
app.patch('/songs/+:id', (req, res) => {
    const { id } = req.params
    const mySongIndex = _.findIndex(data, { id: Number(id) })
    const body = req.body
    console.log('id', id)
    console.log('mySongIndex', mySongIndex)
    console.log('body que me envia el usuarii', body)

    const keys = Object.keys(body)

    console.log('claves a actualizar', keys)

    keys.forEach((key) => {
        console.log('key', key)
        data[mySongIndex][key] = body[key]
    })

    console.log('DATA', data)

    res.send(data)

})

// modificar total una cancion dado un id
app.put('/songs/+:id', (req, res) => {
    const { id } = req.params
    const mySongIndex = _.findIndex(data, { id: Number(id) })
    const body = req.body


    if (body.name === '' || body.name === undefined || body.album === '' || body.album === undefined || body.artist === '' || body.artist === undefined)
        return res.send('Parametros requeridos: name, album, artist')


    const mySong = data[mySongIndex]
    let myUpdate = body
    myUpdate.id = id

    data[mySongIndex] = myUpdate

    console.log('DATA PUT', data)

    res.send(data)

})

app.listen(app.get('port'), (() => {
    console.log('Server running at http://localhost:%d in %s mode', app.get('port'), app.get('env'))
}));